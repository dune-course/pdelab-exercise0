%\documentclass[12pt,a4paper]{article}
\documentclass[american,a4paper]{article}
\usepackage{float}
\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage{uebungen}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{amssymb}

\lstset{language=C++, basicstyle=\ttfamily,
  keywordstyle=\color{black}\bfseries, tabsize=4, stringstyle=\ttfamily,
  commentstyle=\it, extendedchars=true, escapeinside={/*@}{@*/}}

\title{\textbf{DUNE-PDELab Exercise 0}}
\dozent{Peter Bastian, Dominic Kempf, Steffen Müthing}
\institute{}
\semester{}
\abgabetitle{}
%\abgabeinfo{ in der Übung}
\uebungslabel{Exercise}
\blattlabel{CIIT-IWR Workshop 2015}


\newcommand{\vx}{\vec x}
\newcommand{\grad}{\vec \nabla}
\newcommand{\wind}{\vec \beta}
\newcommand{\Laplace}{\Delta}
\newcommand{\mycomment}[1]{}

\begin{document}

\blatt{}{}

\begin{uebung}{\bf Poisson Equation with Piecewise Linear Elements}

In this session you will
\begin{itemize}
\item work on the problems presented in the lecture and see how the
    code works
\item modify the implementation to solve a different problem
    term
\item determine the convergence of the finite element method
\end{itemize}

\begin{enumerate}
\item {\sc Warming up}:\\ The code presented in the lecture solves the following elliptic
    equation with Dirichlet boundary conditions
$$
    \begin{array}{rcll}
      -\Laplace u  & = & f & \text{ in } \Omega, \\
      u & = & g & \text{ on } \partial\Omega,
    \end{array}
$$
where
$$ f(x) = 2 \quad\text{and}\quad  g(x) = \sum_{i=1}^d (x)_i^2.$$

First of all check that $u(x) =  \sum_{i=1}^d (x)_i^2$ is a solution in any dimension $d$.

Start by changing to the directory \lstinline{ciit-dune-course/release-build/pdelab-exercise0/src}
:
\lstset{language=bash}
\begin{lstlisting}
[user@localhost /]$ cd
[user@localhost ~]$ cd ciit-dune-course
[user@localhost ciit-dune-course]$ cd release-build
[user@localhost release-build]$ cd pdelab-exercise0
[user@localhost pdelab-exercise0]$ cd src
\end{lstlisting}
You may now compile the code by running \lstinline{make} and run the code:
\lstset{language=bash}
\begin{lstlisting}
[user@localhost src]$ make
[user@localhost src]$ ./exercise0
\end{lstlisting}

The source file is called \lstinline{exercise0.cc}.

What the program does is controlled by the file \lstinline{exercise0.ini}:
\lstset{language=bash}
\begin{lstlisting}
[grid]
dim=2
refinement=3

[grid.oned]
a=0.0
b=1.0
elements=10

[grid.twod]
filename=unitsquare.msh

[grid.threed]
filename=unitcube.msh

[output]
filename=tuttut
\end{lstlisting}

Here you may change the dimension, the number of mesh refinements
and the names of input and output files. In dimension 1 the extension
\lstinline{.vtp} is added and in dimension 2 and 3 \lstinline{.vtu} is added.

In order to generate a set of output files you may set
\begin{lstlisting}
refinement=0
\end{lstlisting}
and
\begin{lstlisting}
[output]
filename=solution_0
\end{lstlisting}
and then
\begin{lstlisting}
refinement=1
\end{lstlisting}
and
\begin{lstlisting}
[output]
filename=solution_1
\end{lstlisting}
and so on. ParaView will then recognize that the files are related
and you can switch easily from file to file.

In ParaView use the Calculator filter to visualize the difference
$|u-u_h|$ and determine the maximum error.

\item {\sc Solving a new problem}:

Now consider
$$ f(x) = \sum_{i=1}^d 6(x)_i \quad\text{and}\quad  g(x) = \sum_{i=1}^d (x)_i^3$$
and check that $u(x)=\sum_{i=1}^d (x)_i^3$ solves the PDE.

Implement the new $f$ and $g$ function in the files \lstinline{ffunction.hh}
and \lstinline{gfunction.hh} respectively. Recompile and run the program
accordingly.

\item {\sc Analysis of finite element error}:

Produce now a sequence of output files for different levels
of mesh refinement $0, 1, 2, \ldots$. The visualize the error $|u-u_h|$
in ParaView and determine the maximum error on each level.
Make a table and determine the convergence rate.

\item {\sc Use a different solver}:

You may also try to exchange the iterative linear solver by
replacing it with the following lines:
\begin{lstlisting}
typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
LS ls(5000,true);
\end{lstlisting}
Compare the number of iterations which is given by the
following lines in the output:
\begin{lstlisting}
=== CGSolver
   12       1.9016e-09
=== rate=0.144679, T=0.02362, TIT=0.00196833, IT=12
\end{lstlisting}
\end{enumerate}

\end{uebung}

\begin{uebung}{\bf Extending the Local Operator}

Now consider the extended equation of the form
$$
    \begin{array}{rcll}
      -\nabla\cdot (k(x) \nabla u) +a(x) u  & = & f & \text{ in } \Omega, \\
      u & = & g & \text{ on } \partial\Omega,
    \end{array}
$$
with scalar functions $k(x)$, $a(x)$.

\begin{enumerate}

\item In a first step show that the weak formulation of the problem involves
the new bilinear form $$a(u,v) = \int_\Omega k(x) \nabla u(x) \cdot \nabla v(x) + a(x) u(x) v(x) \,dx .$$

\item The main work is now to extend the local operator given by the class
\lstinline{PoissonP1}. This is done in several steps:
\begin{itemize}
\item Copy class \lstinline{PoissonP1} to a new file and rename it.
\item Provide analytic functions for $k(x)$ and $a(x)$ and pass them
to the local operator.
\item Extend the local operator to first handle $k(x)$ by assuming the function $k(x)$
to be {\bf constant} on mesh elements.
\item Now extend the local operator to first handle $a(x) u(x)$. Also assume the function
$a(x)$ to be {\bf constant} on mesh elements.
\end{itemize}

\item Plug in your new local operator in the driver code and test it.

\end{enumerate}


\end{uebung}


\end{document}
